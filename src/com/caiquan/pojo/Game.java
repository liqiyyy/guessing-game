package com.caiquan.pojo;

public class Game {
	
	//将对象作为Game类的一个属性
	String name="***";
	Person person=new Person();
	Computer computer=new Computer();
	int count=0;
	//1.剪刀 2.石头 3.布
	public void startGame() {
		System.out.println("游戏开始！！！");
		System.out.println(person.name+"-----vs-----"+computer.name);
		for(int i=1;i<=5;i++) {
			this.count=i;
			System.out.println("当前是第"+this.count+"轮");
			int pchoice=this.person.chuQuan();
			int cchoice=this.computer.chuQuan();
			if(pchoice==1&&cchoice==2||pchoice==2&&cchoice==3||pchoice==3&&cchoice==1) {
				System.out.println("这一局小智赢了");
				this.computer.score+=1;
			}else if(pchoice==1&&cchoice==3||pchoice==2&&cchoice==1||pchoice==3&&cchoice==2) {
				System.out.println("这一局玩家赢了");
				this.person.score+=1;
			}else if(pchoice==1&&cchoice==1||pchoice==2&&cchoice==2||pchoice==3&&cchoice==3) {
				System.out.println("这一局平手");
			}else if(pchoice==0) {
				System.out.println("这一局玩家输入错误，小智赢了");
				this.computer.score+=1;
			}else if(cchoice==0) {
				System.out.println("这一局小智输入错误，玩家赢了");
				this.person.score+=1;
			}
			System.out.println("当前分数：玩家"+this.person.score+"  电脑"+this.computer.score);
			
			
		}
		getResult();
	}
	
	public void getResult() {
		System.out.println("---最终分数：玩家"+this.person.score+"  电脑"+this.computer.score+"---");
		if(this.person.score>this.computer.score) {
			System.out.println("恭喜您获胜！");
			System.out.println("You win!!!");
		}else if(this.person.score<this.computer.score) {
			System.out.println("很遗憾，电脑获胜");
			System.out.println("Game over!!!");
		}else if(this.person.score==this.computer.score) {
			System.out.println("您和电脑小智达成了平手");
		}
		System.out.println("游戏结束！！！");
	}
	
}
